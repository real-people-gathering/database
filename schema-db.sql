PGDMP     5    0                 x            RPG    11.6    12.0 %    n           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            o           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            p           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            q           1262    16393    RPG    DATABASE     �   CREATE DATABASE "RPG" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "RPG";
                postgres    false                        3079    16394 	   uuid-ossp 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    DROP EXTENSION "uuid-ossp";
                   false            r           0    0    EXTENSION "uuid-ossp"    COMMENT     W   COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';
                        false    2            �            1259    16405    Reward    TABLE     v   CREATE TABLE public."Reward" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    label character varying
);
    DROP TABLE public."Reward";
       public            postgres    false    2            �            1259    16412    Badge    TABLE     �   CREATE TABLE public."Badge" (
    id uuid DEFAULT public.uuid_generate_v4(),
    "X" integer NOT NULL,
    "Y" integer NOT NULL,
    "idSpritePlank" uuid NOT NULL
)
INHERITS (public."Reward");
    DROP TABLE public."Badge";
       public            postgres    false    2    197            �            1259    16419    Comment    TABLE     �   CREATE TABLE public."Comment" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    content text NOT NULL,
    "createdAt" date NOT NULL,
    "deletedAt" date,
    "idPost" uuid NOT NULL
);
    DROP TABLE public."Comment";
       public            postgres    false    2            �            1259    16426    Emote    TABLE     �   CREATE TABLE public."Emote" (
    id uuid DEFAULT public.uuid_generate_v4(),
    "imageLink" text
)
INHERITS (public."Reward");
    DROP TABLE public."Emote";
       public            postgres    false    2    197            �            1259    16433    HistoryComment    TABLE     �   CREATE TABLE public."HistoryComment" (
    "idUser" uuid NOT NULL,
    "idComment" uuid NOT NULL,
    "modifiedAt" date NOT NULL,
    content text NOT NULL
);
 $   DROP TABLE public."HistoryComment";
       public            postgres    false            �            1259    16439    HistoryLevel    TABLE        CREATE TABLE public."HistoryLevel" (
    "idUser" uuid NOT NULL,
    "idLevel" uuid NOT NULL,
    "reachedAt" date NOT NULL
);
 "   DROP TABLE public."HistoryLevel";
       public            postgres    false            �            1259    16442    Image    TABLE     �   CREATE TABLE public."Image" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    imagelink text NOT NULL,
    "createdAt" date NOT NULL,
    "deletedAt" date NOT NULL,
    "idPost" uuid NOT NULL
);
    DROP TABLE public."Image";
       public            postgres    false    2            �            1259    16449    Level    TABLE     �   CREATE TABLE public."Level" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    number integer NOT NULL,
    "requiredXP" integer NOT NULL
);
    DROP TABLE public."Level";
       public            postgres    false    2            �            1259    16453    Post    TABLE     �   CREATE TABLE public."Post" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    content text NOT NULL,
    "createdAt" date DEFAULT CURRENT_DATE NOT NULL,
    "deletedAt" date,
    "idUser" uuid
);
    DROP TABLE public."Post";
       public            postgres    false    2            �            1259    16461    Reaction    TABLE     x   CREATE TABLE public."Reaction" (
    "idUser" uuid NOT NULL,
    "idPost" uuid NOT NULL,
    "idEmote" uuid NOT NULL
);
    DROP TABLE public."Reaction";
       public            postgres    false            �            1259    16464    RelationStatus    TABLE     z   CREATE TABLE public."RelationStatus" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    label text NOT NULL
);
 $   DROP TABLE public."RelationStatus";
       public            postgres    false    2            �            1259    16471    Relationship    TABLE     �   CREATE TABLE public."Relationship" (
    "idTargetUser" uuid NOT NULL,
    "idRelationshipStatus" uuid NOT NULL,
    "idCurrentUser" uuid NOT NULL
);
 "   DROP TABLE public."Relationship";
       public            postgres    false            �            1259    16474    SpritePlank    TABLE     }   CREATE TABLE public."SpritePlank" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "imageLink" text NOT NULL
);
 !   DROP TABLE public."SpritePlank";
       public            postgres    false    2            �            1259    16481    User    TABLE       CREATE TABLE public."User" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    fullname character varying,
    mail character varying,
    phone character varying,
    username character varying,
    "currentXP" integer,
    "currentLevel" integer
);
    DROP TABLE public."User";
       public            postgres    false    2            s           0    0    TABLE "User"    COMMENT     E   COMMENT ON TABLE public."User" IS 'This is the table for user info';
          public          postgres    false    210            �            1259    16488    Video    TABLE     �   CREATE TABLE public."Video" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "videoLink" text NOT NULL,
    "createdAt" date NOT NULL,
    "deletedAt" date NOT NULL,
    "idPost" uuid NOT NULL
);
    DROP TABLE public."Video";
       public            postgres    false    2            �
           2606    16496    Badge Badge_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Badge"
    ADD CONSTRAINT "Badge_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Badge" DROP CONSTRAINT "Badge_pkey";
       public            postgres    false    198            �
           2606    16498    Comment Comment_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT "Comment_pkey" PRIMARY KEY (id);
 B   ALTER TABLE ONLY public."Comment" DROP CONSTRAINT "Comment_pkey";
       public            postgres    false    199            �
           2606    16500    Emote Emote_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Emote"
    ADD CONSTRAINT "Emote_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Emote" DROP CONSTRAINT "Emote_pkey";
       public            postgres    false    200            �
           2606    16502 "   HistoryComment HistoryComment_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public."HistoryComment"
    ADD CONSTRAINT "HistoryComment_pkey" PRIMARY KEY ("idUser", "idComment");
 P   ALTER TABLE ONLY public."HistoryComment" DROP CONSTRAINT "HistoryComment_pkey";
       public            postgres    false    201    201            �
           2606    16504    HistoryLevel HistoryLevel_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY public."HistoryLevel"
    ADD CONSTRAINT "HistoryLevel_pkey" PRIMARY KEY ("idUser", "idLevel");
 L   ALTER TABLE ONLY public."HistoryLevel" DROP CONSTRAINT "HistoryLevel_pkey";
       public            postgres    false    202    202            �
           2606    16506    Image Image_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Image"
    ADD CONSTRAINT "Image_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Image" DROP CONSTRAINT "Image_pkey";
       public            postgres    false    203            �
           2606    16508    Level Level_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Level"
    ADD CONSTRAINT "Level_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Level" DROP CONSTRAINT "Level_pkey";
       public            postgres    false    204            �
           2606    16510    Post Post_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."Post"
    ADD CONSTRAINT "Post_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."Post" DROP CONSTRAINT "Post_pkey";
       public            postgres    false    205            �
           2606    16512    Reaction Reaction_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public."Reaction"
    ADD CONSTRAINT "Reaction_pkey" PRIMARY KEY ("idUser", "idPost", "idEmote");
 D   ALTER TABLE ONLY public."Reaction" DROP CONSTRAINT "Reaction_pkey";
       public            postgres    false    206    206    206            �
           2606    16514 "   RelationStatus RelationStatus_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."RelationStatus"
    ADD CONSTRAINT "RelationStatus_pkey" PRIMARY KEY (id);
 P   ALTER TABLE ONLY public."RelationStatus" DROP CONSTRAINT "RelationStatus_pkey";
       public            postgres    false    207            �
           2606    16516    Relationship Relationship_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public."Relationship"
    ADD CONSTRAINT "Relationship_pkey" PRIMARY KEY ("idTargetUser", "idCurrentUser");
 L   ALTER TABLE ONLY public."Relationship" DROP CONSTRAINT "Relationship_pkey";
       public            postgres    false    208    208            �
           2606    16518    Reward Reward_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public."Reward"
    ADD CONSTRAINT "Reward_pkey" PRIMARY KEY (id);
 @   ALTER TABLE ONLY public."Reward" DROP CONSTRAINT "Reward_pkey";
       public            postgres    false    197            �
           2606    16520    SpritePlank SpritePlank_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."SpritePlank"
    ADD CONSTRAINT "SpritePlank_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."SpritePlank" DROP CONSTRAINT "SpritePlank_pkey";
       public            postgres    false    209            �
           2606    16522    User User_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."User" DROP CONSTRAINT "User_pkey";
       public            postgres    false    210            �
           2606    16524    Video Video_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Video"
    ADD CONSTRAINT "Video_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Video" DROP CONSTRAINT "Video_pkey";
       public            postgres    false    211           